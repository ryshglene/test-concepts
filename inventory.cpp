#include <iostream>
#include <string>

using namespace std;

class UserInterface {
    public:
        void welcomeScreen() {
            cout << "\n\tWelcome to the Program!\n" << endl;
        }
        string askName() {
            string name;
            cout << "Enter name: ";
            cin >> name;
            return name;
        }
        string askPass() {
            string password;
            cout << "Enter password: ";
            cin >> password;
            return password;
        }
        string askCommand() {
            string command;
            cout << "Command (? for help): " ;
            cin >> command;
            return command;
        }
};

class SessionManager {
    private:
        class AdminAction {
            public:
                void view() {
                    cout << "Viewing databases..." << endl;
                }
                void create() {
                    cout << "Creating databases..." << endl;
                }
                void remove() {
                    cout << "Removing databases..." << endl;
                }
                void update() {
                    cout << "Updating databases..." << endl;
                }
                void exitCmd() {
                    exit(0);
                }
                void help() {
                    cout << "v   View items on the database." << endl;
                    cout << "c   Create items on the database." << endl;
                    cout << "r   Remove items on the database." << endl;
                    cout << "u   Update items on the database." << endl;
                    cout << "q   Update items on the database." << endl;
                    cout << "?   Show this help menu." << endl;
                }
        };
        class UserAction {
            public:
                void view() {
                    cout << "Viewing databases..." << endl;
                }
                void exitCmd() {
                    exit(0);
                }
                void help() {
                    cout << "v   View items on the database." << endl;
                    cout << "q   Update items on the database." << endl;
                    cout << "?   Show this help menu." << endl;
                }
        };
        string passwordRaw = "idk";
        bool isCorrect(string givenPass) {
            if (givenPass.compare(passwordRaw) == 0) {
                return true;
            }
            else {
                return false;
            }
        }
        bool isAuthenticated = false;
        bool isAdmin = false;
    public:
        UserInterface interface;
        SessionManager() {
            // where everything happens...
            interface.welcomeScreen();
            while (!isLoggedIn()) {
                authenticate(interface.askName());
            }
            // GUIs don't need while loops for input
            if (isLoggedInAsAdmin()) {
                cout << "Hello Admin!\n" << endl;
                AdminAction action;
                while (true) {
                    string cmd = interface.askCommand();
                    if (cmd.compare("v") == 0) {
                        action.view();
                    }
                    else if (cmd.compare("c") == 0) {
                        action.create();
                    }
                    else if (cmd.compare("r") == 0) {
                        action.remove();
                    }
                    else if (cmd.compare("u") == 0) {
                        action.update();
                    }
                    else if (cmd.compare("q") == 0) {
                        action.exitCmd();
                    }
                    else if (cmd.compare("?") == 0) {
                        action.help();
                    }
                }
            }
            else {
                cout << "Hello User!\n" << endl;
                UserAction action;
                while (true) {
                    string cmd = interface.askCommand();
                    if (cmd.compare("v") == 0) {
                        action.view();
                    }
                    else if (cmd.compare("q") == 0) {
                        action.exitCmd();
                    }
                    else if (cmd.compare("?") == 0) {
                        action.help();
                    }
                }
            }
        }
        void authenticate(string name) {
            if (name.compare("admin") == 0) {
                string pass = interface.askPass();
                if (isCorrect(pass)) {
                    isAuthenticated = true;
                    isAdmin = true;
                }
            }
            else {
                isAuthenticated = true;
            }
        }
        bool isLoggedIn() {
            if (isAuthenticated) {
                cout << "You are logged in." << endl;
                return true;
            }
            else {
                cout << "You are NOT logged in." << endl;
                return false;
            }
        }
        bool isLoggedInAsAdmin() {
            if (isAdmin) {
                cout << "You are an admin." << endl;
                return true;
            }
            else {
                cout << "You are NOT an admin." << endl;
                return false;
            }
        }
};

int main() {
    SessionManager newSession;

    return 0;
}
