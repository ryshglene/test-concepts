#! /bin/sh

set -euo pipefail

#
# First you gotta pick a primary key
# that you can use for iteration
#
# here, I'll be using the ISBN for each book as a primary key

isbn_list="$(seq -f "%013g" 46 69)" # this should be a hard coded list
                                    # but I'll just auto-generate it for simplicity

# files we'll be working with:
data_file="$(dirname $0)/books-data.txt"
printf "%s..\n" "Using data from $(basename $data_file)."
db_file="$(dirname $0)/books.db"

# always recreate the database, bc this is a sample
[ -f "$db_file" ] && rm "$db_file"
cat << EOF | sqlite3 "$db_file"
create table AllBooks (ISBN varchar(255) primary key, Title varchar(255), Author varchar(255), Publisher varchar(255));
.exit
EOF

printf "%s..\n" "Creating database $(basename $db_file)."
for isbn in $isbn_list; do
    # here you can extract data using your primary key,
    # with the help of a bunch of core utils
    #
    # jus make sure your data file is properly formatted

    title="$(grep -A1 $isbn books-data.txt | tail -n 1)"
    author="$(grep -A2 $isbn books-data.txt | tail -n 1)"
    publisher="$(grep -A3 $isbn books-data.txt | tail -n 1)"

    cat << EOF | sqlite3 "$db_file"
insert into AllBooks values ('$isbn', '$title', '$author', '$publisher');
.exit
EOF
    
done

printf "%s\n" "You can now inspect $(basename $db_file) for the inserted data."
